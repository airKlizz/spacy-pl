import spacy

from elg import FlaskService
from elg.model import Annotation, AnnotationsResponse


class SpacyPL(FlaskService):

    nlp = spacy.load('pl_spacy_model_morfeusz')

    def process_text(self, content):
        doc = self.nlp(content.content)
        tokens = []
        for token in doc:
            tokens.append(
                Annotation(
                    start=token.idx,
                    end=token.idx+len(token),
                    features={
                        "text": token.text,
                        "lemma": token.lemma_,
                        "pos": token.pos_,
                    },
                )
            )

        response = AnnotationsResponse(annotations={"tokens": tokens})
        return response

flask_service = SpacyPL("SpacyPL")
app = flask_service.app